<?php

namespace Vagas;

use Zend\Json\Server\Client;
use Zend\Mvc\ModuleRouteListener;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'vagas_search_form' => 'Vagas\Form\Search'
            ),
            'factories' => array(
                'vagas_vaga_api' => function($services) {
                    $config = $services->get('Config');
                    $client = new Client($config['vagas']['service_uri']);
                    $service = new Service\VagaApi($client);
                    return $service;
                },
            )
        );
    }

    public function getControllerConfig()
    {
        return array(
            'factories' => array(
                'Vagas\Controller\Vagas' => function($controllers) {
                    $services = $controllers->getServiceLocator();
                    return new Controller\VagasController(
                        $services->get('vagas_vaga_api'),
                        $services->get('vagas_search_form')
                    );
                }
            )
        );
    }
}
