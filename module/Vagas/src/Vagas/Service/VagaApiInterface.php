<?php

namespace Vagas\Service;

interface VagaApiInterface
{
    public function findAll();
    public function searchBy(array $criteria);
}