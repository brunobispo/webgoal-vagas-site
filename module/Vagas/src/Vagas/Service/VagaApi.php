<?php

namespace Vagas\Service;

use Zend\Json\Server\Client;

class VagaApi implements VagaApiInterface
{
    protected $rpcClient;

    public function __construct(Client $rpcClient)
    {
        $this->setRpcClient($rpcClient);
    }

    public function findAll()
    {
        return $this->getRpcClient()->call('findAll');
    }

    public function searchBy(array $criteria)
    {
        return $this->getRpcClient()->call('searchBy', array('criteria' => $criteria));
    }
    
    public function setRpcClient(Client $rpcClient)
    {
        $this->rpcClient = $rpcClient;
    }
    
    public function getRpcClient()
    {
        return $this->rpcClient;
    }
}