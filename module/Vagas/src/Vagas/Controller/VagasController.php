<?php

namespace Vagas\Controller;


use Zend\Form\FormInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Vagas\Service\VagaApiInterface as VagaApi;

class VagasController extends AbstractActionController
{
    protected $service;
    protected $searchForm;

    function __construct(VagaApi $service, FormInterface $searchForm)
    {
        $this->service = $service;
        $this->searchForm = $searchForm;
    }

    public function indexAction()
    {
        $entities = $this->service->findAll();

        $result = new ViewModel(array('entities' => $entities, 'searchForm' => $this->searchForm));
        if ($this->request->isXmlHttpRequest()) {
            $result->setTerminal(true);
            $result->setTemplate('vagas/vagas/list');
        }

        return $result;
    }

    public function searchAction()
    {
        $searchForm = $this->searchForm;

        if ($this->request->isPost()) {
            $searchForm->setData($this->params()->fromPost());
            if (!$searchForm->isValid()) {
                $this->redirect()->toRoute('vagas');
            }
            return $this->redirect()->toRoute(null, $searchForm->getData());
        }

        $searchForm->setData(!count($this->params()->fromQuery()) ? $this->params()->fromRoute() : $this->params()->fromQuery());
        if (!$searchForm->isValid()) {
            $this->redirect()->toRoute('vagas');
        }

        $criteria = array_filter($searchForm->getData());
        if (empty($criteria)) {
            $this->redirect()->toRoute('vagas');
        }

        $entities = $this->service->searchBy($criteria);

        $result = new ViewModel(compact('entities', 'searchForm'));
        if ($this->request->isXmlHttpRequest()) {
            $result->setTerminal(true);
            $result->setTemplate('vagas/vagas/list');
        } else {
            $result->setTemplate('vagas/vagas/index');
        }

        return $result;
    }
}