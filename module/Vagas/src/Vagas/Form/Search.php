<?php
/**
 * Formulário de busca
 * User: Bruno
 * Date: 24/06/13
 * Time: 20:06
 */

namespace Vagas\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

class Search extends Form
    implements InputFilterProviderInterface
{
    function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);

        $this->setLabel('Pesquisa');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'titulo_descricao',
            'attributes' => array(
                'placeholder' => 'Título ou descrição'
            )
        ));

        $this->add(array(
            'name' => 'beneficios',
            'attributes' => array(
                'placeholder' => 'Benefícios'
            )
        ));

        $this->add(array(
            'name' => 'requisitos',
            'attributes' => array(
                'placeholder' => 'Requisitos'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Buscar'
            ),
            'attributes' => array(
                'type' => 'submit'
            )
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'titulo_descricao' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'Null')
                )
            ),
            'beneficios' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'Null')
                )
            ),
            'requisitos' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'Null')
                )
            ),
            'submit' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'Null')
                )
            )
        );
    }


}