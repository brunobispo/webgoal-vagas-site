<?php
/**
 * Elemento select de vagas
 */

namespace Candidatos\Form\Element;


use Zend\Form\Element\Select;
use Candidatos\Service\CandidatoApiInterface as CandidatoApi;
use Zend\Form\Element\MultiCheckbox;

class ConhecimentoMultiCheckbox extends MultiCheckbox
{
    protected $service;

    public function setOptions($options)
    {
        $result = parent::setOptions($options);

        if (isset($options['service'])) {
            $this->setService($options['service']);
        }

        return $result;
    }

    public function setService(CandidatoApi $service)
    {
        $this->service = $service;
    }

    public function getService()
    {
        return $this->service;
    }

    public function getEmptyOption()
    {
        return 'Selecione uma vaga';
    }

    public function getValueOptions()
    {
        $entities = $this->getService()->findAllConhecimentos();
        $map = array();
        foreach ($entities as $entity) {
            $map[$entity['id']] = $entity['nome'];
        }

        return $map;
    }

    public function setValue($value)
    {
        if (is_array($value)) {
            parent::setValue($value['id']);
        } else {
            parent::setValue($value);
        }
    }


}