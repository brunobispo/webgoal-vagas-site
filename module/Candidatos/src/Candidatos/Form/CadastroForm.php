<?php
/**
 * Formulário de cadastro de candidatos
 */

namespace Candidatos\Form;


use Zend\Form\Form;
use Vagas\Service\VagaApiInterface as VagaApi;
use Candidatos\Service\CandidatoApiInterface as CandidatoApi;

class CadastroForm extends Form
{
    public function __construct(CandidatoApi $candidatosService, VagaApi $vagasService, $name = null, $options = array())
    {
        parent::__construct($name, $options);
        
        $this->setLabel('Cadastro');

        $this->add(array(
            'name' => 'nome',
            'options' => array(
                'label' => 'Nome',
            ),
            'attributes' => array(
                'required' => true,
                'placeholder' => 'Ex. João Paulo'
            )
        ));

        $this->add(array(
            'name' => 'nascimento',
            'type' => 'date',
            'options' => array(
                'label' => 'Data de nascimento',
            ),
            'attributes' => array(
                'required' => true,
                'placeholder' => 'Ex. 16/04/1980'
            )
        ));

        $this->add(array(
            'name' => 'ondeEstudou',
            'type' => 'textarea',
            'options' => array(
                'label' => 'Onde estudou',
            ),
            'attributes' => array(
                'required' => true,
                'rows' => 3,
                'placeholder' => 'Escreva sobre as escolas e cursos'
            )
        ));

        $this->add(array(
            'name' => 'ondeTrabalhou',
            'type' => 'textarea',
            'options' => array(
                'label' => 'Onde trabalhou',
            ),
            'attributes' => array(
                'required' => true,
                'rows' => 3,
                'placeholder' => 'Escreva sobre suas experiências'
            )
        ));

        $this->add(array(
            'name' => 'experiencia',
            'type' => 'textarea',
            'options' => array(
                'label' => 'Descrição dos conhecimentos',
            ),
            'attributes' => array(
                'required' => true,
                'rows' => 3,
                'placeholder' => 'Escreva sobre seus conhecimentos'
            )
        ));

        $this->add(array(
            'name' => 'pretensaoSalarial',
            'type' => 'text',
            'options' => array(
                'label' => 'Pretensão Salarial',
            ),
            'attributes' => array(
                'required' => true,
            )
        ));

        $this->add(array(
            'name' => 'conhecimentos',
            'type' => 'Candidatos\Form\Element\ConhecimentoMultiCheckbox',
            'options' => array(
                'label' => 'conhecimentos',
                'service' => $candidatosService
            ),
            'attributes' => array(
                'required' => false,
            )
        ));


        $this->add(array(
            'name' => 'vaga',
            'type' => 'Candidatos\Form\Element\VagaSelect',
            'options' => array(
                'label' => 'Vaga',
                'service' => $vagasService
            ),
            'attributes' => array(
                'required' => false,
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Enviar cadastro'
            ),
            'attributes' => array(
                'type' => 'submit'
            )
        ));
    }



}