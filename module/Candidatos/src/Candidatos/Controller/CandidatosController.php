<?php

namespace Candidatos\Controller;


use Zend\Form\FormInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Candidatos\Service\CandidatoApiInterface as CandidatoApi;

class CandidatosController extends AbstractActionController
{
    protected $service;
    protected $cadastroForm;

    function __construct(CandidatoApi $service, FormInterface $cadastroForm)
    {
        $this->service = $service;
        $this->cadastroForm = $cadastroForm;
    }

    function cadastroAction()
    {
        $cadastroForm = $this->cadastroForm;
        $message = null;

        if ($this->request->isPost()) {
            $cadastroForm->setData($this->params()->fromPost());
            $result = $this->service->create($this->params()->fromPost());
            if (isset($result['errorMessages'])) {
                $cadastroForm->setMessages($result['errorMessages']);
                $message = array('message' => '<strong>Houveram erros ao tentar enviar seu cadastro.</strong> Verifique os campos destacados:', 'type' => 'error');
            } else {
                $cadastroForm = null;
                $message = array('message' => 'Cadastro efetuado com sucesso', 'type' => 'success');
            }
        }

        return compact('cadastroForm', 'message');
    }
}