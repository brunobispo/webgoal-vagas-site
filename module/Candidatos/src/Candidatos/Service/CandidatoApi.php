<?php

namespace Candidatos\Service;

use Zend\Json\Server\Client;

class CandidatoApi implements CandidatoApiInterface
{
    protected $rpcClient;

    public function __construct(Client $rpcClient)
    {
        $this->setRpcClient($rpcClient);
    }

    public function create(array $data)
    {
        return $this->getRpcClient()->call('create', array('data' => $data));
    }

    public function findAllConhecimentos()
    {
        return $this->getRpcClient()->call('findAllConhecimentos');
    }
    
    public function setRpcClient(Client $rpcClient)
    {
        $this->rpcClient = $rpcClient;
    }
    
    public function getRpcClient()
    {
        return $this->rpcClient;
    }
}