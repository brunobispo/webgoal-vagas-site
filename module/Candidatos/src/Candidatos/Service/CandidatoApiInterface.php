<?php

namespace Candidatos\Service;

interface CandidatoApiInterface
{
    public function create(array $data);
}