<?php

namespace Candidatos;

use Zend\Json\Server\Client;
use Zend\Mvc\ModuleRouteListener;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'candidatos_candidato_api' => function($services) {
                    $config = $services->get('Config');
                    $client = new Client($config['candidatos']['service_uri']);
                    $service = new Service\CandidatoApi($client);
                    return $service;
                },
                'candidatos_cadastro_form' => function($services) {
                    return new Form\CadastroForm($services->get('candidatos_candidato_api'), $services->get('vagas_vaga_api'));
                },
            ),
        );
    }

    public function getControllerConfig()
    {
        return array(
            'factories' => array(
                'Candidatos\Controller\Candidatos' => function($controllers) {
                    $services = $controllers->getServiceLocator();
                    return new Controller\CandidatosController(
                        $services->get('candidatos_candidato_api'),
                        $services->get('candidatos_cadastro_form')
                    );
                }
            )
        );
    }
}
