<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    // @TODO atualizar endereço quando hospedá-lo publicamente
    'vagas' => array(
        'service_uri' => 'http://vagas-api.webgoal.web687.uni5.net/vagas'
    ),
    'candidatos' => array(
        'service_uri' => 'http://vagas-api.webgoal.web687.uni5.net/candidatos'
    )
);
